#!/usr/bin/env bash

. release_info
rm -f release
rm -rf public
curl $RELEASE_URL -o release
chmod a+x release
sed -i 's/GA_ID/'"$GA"'/' site/config.toml
mkdir -p site/layouts/shortcodes
echo $RELEASE_DESC > site/layouts/shortcodes/release-desc.html
echo $RELEASE_URL > site/layouts/shortcodes/release-url.html
./release short-version > site/layouts/shortcodes/version.html
./release help > site/layouts/shortcodes/help.html
./release help next-version > site/layouts/shortcodes/next-version.html
./release help changelog > site/layouts/shortcodes/changelog.html
./release help tag > site/layouts/shortcodes/tag.html
./release help commit-and-tag > site/layouts/shortcodes/commit-and-tag.html
./release help tag-and-commit > site/layouts/shortcodes/tag-and-commit.html
./release help add-download > site/layouts/shortcodes/add-download.html
./release help add-download-link > site/layouts/shortcodes/add-download-link.html
./release help test-git > site/layouts/shortcodes/test-git.html
./release help test-api > site/layouts/shortcodes/test-api.html