---
title: "Docker image"
anchor: "docker"
weight: 50
---

`docker pull registry.gitlab.com/juhani/go-semrel-gitlab:{{% version %}}`

### How to use the image in `.gitlab-ci.yml`

```yaml
trigger-release:
  stage: release
  image: registry.gitlab.com/juhani/go-semrel-gitlab:{{% version %}}
  script:
    - release changelog
    - release commit-and-tag CHANGELOG.md
  when: manual
  except:
    - tags
```

Check the project source for a
[full example](https://gitlab.com/juhani/go-semrel-gitlab/blob/master/.gitlab-ci.yml)