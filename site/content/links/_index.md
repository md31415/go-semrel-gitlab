---
title: "Links"
anchor: "links"
weight: 60
---

- go-semrel-gitlab [project](https://gitlab.com/juhani/go-semrel-gitlab)
- semantic-release [project](https://github.com/semantic-release/semantic-release)
  and [documentation](https://semantic-release.gitbooks.io/semantic-release/content)
- [AngularJS commit conventions](https://gist.github.com/stephenparish/9941e89d80e2bc58a153#file-commit-md)
- Semantic versioning [2.0.0](https://semver.org/spec/v2.0.0.html) and [1.0.0](https://semver.org/spec/v1.0.0.html)