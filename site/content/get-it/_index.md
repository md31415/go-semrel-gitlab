---
title: "Download"
anchor: "download"
weight: 15
---

> ----
> 
> **NOTE:** a major refactor was done [from 0.13.0 to 0.14.0](https://gitlab.com/juhani/go-semrel-gitlab/compare/v0.13.0...v0.14.0?view=parallel)
> to handle some cases of temporary failures with gitlab api. Functionality *should not* have changed,
> but it's possible that something slipped through. If you face problems, please
> [submit an issue](https://gitlab.com/juhani/go-semrel-gitlab/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)
> and use [v0.13.0](https://gitlab.com/juhani/go-semrel-gitlab/tags/v0.13.0) until I fix it.
> 
> ----

#### Binary

[{{% release-desc %}} ({{% version %}})]({{% release-url %}})

#### Docker image

`docker pull registry.gitlab.com/juhani/go-semrel-gitlab:{{% version %}}`


