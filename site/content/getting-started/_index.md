---
title: "Getting started"
anchor: "getting-started"
weight: 30
---

1. [add a little rigor to your commit messages](https://semantic-release.gitbooks.io/semantic-release/content/#commit-message-format)
1. configure 'Secret Variables' in your CI / CD settings
  - GL_TOKEN: [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
    to use gitlab api
  - GL_URL: not needed if you're using https://gitlab.com/api/v4/
1. add release tasks to your `.gitlab-ci.yml`
  - [example with binary](https://gitlab.com/juhani/go-semrel-gitlab/blob/8a0163b6bed3d44de3a5804d6ea4a234e2ac79ac/.gitlab-ci.yml#L81-83)
  - [example with docker image](https://gitlab.com/juhani/go-semrel-gitlab/blob/d3c3e8a7c8510f6fa83f5a41d9370ee81df40c84/.gitlab-ci.yml#L29-38)
