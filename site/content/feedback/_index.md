---
title: "Feedback"
anchor: "feedback"
weight: 35
---

**Please, let me know what you think!**

If you have Gitlab account, you can submit [issues](https://gitlab.com/juhani/go-semrel-gitlab/issues).

Otherwise send email to {{% issue-email %}}.
<br />
Issues created with email won't be publicly visible.
