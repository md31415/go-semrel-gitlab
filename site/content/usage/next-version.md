---
title: "next-version"
anchor: "next-version"
weight: 41
---

__`$ release help next-version`__

```
{{% next-version %}}
```