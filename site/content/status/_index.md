---
title: "Recent improvements"
anchor: "changes"
weight: 32
---

[**v0.15.0**](https://gitlab.com/juhani/go-semrel-gitlab/tags/v0.15.0):

- fix the brain fart of the previous release

[**v0.14.0**](https://gitlab.com/juhani/go-semrel-gitlab/tags/v0.14.0):

- **WARNING:** this release has seriously misleading option names for commit type customization, please use v0.15.0 or newer
- recover from some temporary errors with gitlab api
- try to undo performed actions in gitlab, in case of unrecoverable error
- print helpful error messages if manual fixup is required
- options to customize mappings between commit and bump types

[**v0.13.0**](https://gitlab.com/juhani/go-semrel-gitlab/tags/v0.13.0):

- Command to add download link instead of using gitlab uploads: [`'release add-download-link'`](#add-download-link).<br/>

[**v0.12.0**](https://gitlab.com/juhani/go-semrel-gitlab/tags/v0.12.0):

- Add alternative ordering for release: [`'release tag-and-commit'`](#tag-and-commit).<br/>

[**v0.11.7**](https://gitlab.com/juhani/go-semrel-gitlab/tags/v0.11.7):

- Fix `'release next-version -p'` version determination.<br/>

[**v0.11.5**](https://gitlab.com/juhani/go-semrel-gitlab/tags/v0.11.5):

- Improve performance with large repos and lot of merges.

[Full changelog](https://gitlab.com/juhani/go-semrel-gitlab/blob/master/CHANGELOG.md)