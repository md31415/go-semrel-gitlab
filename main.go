/*
go-semrel-gitlab provides tools to automate parts of release process on Gitlab CI

More documentation can be found at https://juhani.gitlab.io/go-semrel-gitlab/
*/
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime/pprof"
	"strings"

	"github.com/juranki/go-semrel/angularcommit"
	"github.com/juranki/go-semrel/inspectgit"
	"github.com/juranki/go-semrel/semrel"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/juhani/go-semrel-gitlab/pkg/actions"
	"gitlab.com/juhani/go-semrel-gitlab/pkg/gitlabutil"
	"gitlab.com/juhani/go-semrel-gitlab/pkg/render"
	"gitlab.com/juhani/go-semrel-gitlab/pkg/workflow"
)

var (
	version string
)

func clientFromContext(c *cli.Context) (*gitlab.Client, error) {
	return gitlabutil.NewClient(
		c.GlobalString("token"),
		c.GlobalString("gl-api"),
		c.GlobalBool("skip-ssl-verify"))
}

func parseCommitTypes(s string) []string {
	ss := strings.Split(strings.ToLower(s), ",")
	rv := []string{}
	for _, t := range ss {
		rv = append(rv, strings.TrimSpace(t))
	}
	return rv
}

func analyzeCommits(c *cli.Context) (*semrel.ReleaseData, error) {
	var patchTypes []string
	var minorTypes []string

	patchTypesString := c.GlobalString("patch-commit-types")
	minorTypesString := c.GlobalString("minor-commit-types")

	if len(patchTypesString) == 0 {
		patchTypes = parseCommitTypes("fix,refactor,perf,docs,style,test")
	} else {
		patchTypes = parseCommitTypes(patchTypesString)
	}
	if len(minorTypesString) == 0 {
		minorTypes = parseCommitTypes("feat")
	} else {
		minorTypes = parseCommitTypes(minorTypesString)
	}

	vcsData, err := inspectgit.VCSData(".")
	if err != nil {
		return nil, err
	}

	return semrel.Release(vcsData, angularcommit.NewWithOptions(&angularcommit.Options{
		FixTypes:     patchTypes,
		FeatureTypes: minorTypes,
		BreakingChangeMarkers: []string{
			`BREAKING\s+CHANGE:`,
			`BREAKING\s+CHANGE`,
			`BREAKING:`,
		},
	}))
}

// release short-version
func shortVersion(c *cli.Context) error {
	fmt.Print(version)
	return nil
}

// release test-api
func testAPI(c *cli.Context) error {
	client, err := clientFromContext(c)
	if err != nil {
		return errors.Wrap(err, "Unable to connect")
	}
	check := actions.NewCheck(client)
	err = workflow.Apply([]workflow.Action{check})
	if err == nil {
		fmt.Println("Connection OK!")
	}
	return err
}

// release add-download
func addDownload(c *cli.Context) error {
	tag := c.GlobalString("ci-commit-tag")
	project := c.GlobalString("ci-project-path")
	file := c.String("f")
	description := c.String("d")

	if len(tag) == 0 {
		return errors.New("add-download is only possible on tag pipeline")
	}
	if len(file) == 0 || len(description) == 0 {
		return errors.New("filename and description must be specified")
	}

	client, err := clientFromContext(c)
	if err != nil {
		return errors.Wrap(err, "Unable to connect")
	}
	getTag := actions.NewGetTag(client, project, tag)
	upload := actions.NewUpload(client, project, file)
	addLink := actions.NewAddLink(&actions.AddLinkParams{
		Client:          client,
		Project:         project,
		LinkDescription: description,
		MDLinkFunc:      upload.MDLinkFunc(),
		TagFunc:         getTag.TagFunc(),
	})
	return workflow.Apply([]workflow.Action{getTag, upload, addLink})
}

// release add-download-link
func addDownloadLink(c *cli.Context) error {
	project := c.GlobalString("ci-project-path")
	tag := c.GlobalString("ci-commit-tag")
	url := c.String("url")
	name := c.String("name")
	description := c.String("description")

	if len(tag) == 0 {
		return errors.New("add-download is only possible on tag pipeline")
	}
	if len(name) == 0 || len(url) == 0 || len(description) == 0 {
		return errors.New("name, url and description must be specified")
	}

	client, err := clientFromContext(c)
	if err != nil {
		return errors.Wrap(err, "Unable to connect")
	}
	getTag := actions.NewGetTag(client, project, tag)
	addLink := actions.NewAddLink(
		&actions.AddLinkParams{
			Client:          client,
			Project:         project,
			LinkDescription: description,
			MDLinkFunc:      func() string { return fmt.Sprintf("[%s](%s)", name, url) },
			TagFunc:         getTag.TagFunc(),
		},
	)
	return workflow.Apply([]workflow.Action{getTag, addLink})
}

// release commit-and-tag
// release tag-and-commit
func commitAndTagBase(c *cli.Context, reversed bool) error {
	branch := c.GlobalString("ci-commit-ref-name")
	project := c.GlobalString("ci-project-path")
	refFunc := actions.FuncOfString(c.GlobalString("ci-commit-sha"))
	files := c.Args()

	info, err := analyzeCommits(c)
	if err != nil {
		return err
	}
	if info.BumpLevel == semrel.NoBump {
		return errors.New("no changes found in commit messages")
	}
	releaseNote, err := render.ReleaseNote(info)
	if err != nil {
		return err
	}
	tagID := "v" + info.NextVersion.String()
	message := fmt.Sprintf("chore: version bump for v%s [skip ci]", info.NextVersion.String())

	client, err := clientFromContext(c)
	if err != nil {
		return errors.Wrap(err, "Unable to connect")
	}

	commit := actions.NewCommit(client, project, files, message, branch)
	if !reversed {
		refFunc = commit.RefFunc()
	}
	tag := actions.NewCreateTag(client, project, refFunc, tagID, releaseNote)
	workflowActions := []workflow.Action{commit, tag}
	if !reversed {
		workflowActions = append(workflowActions, actions.NewCreatePipeline(client, project, actions.FuncOfString(tagID)))
	}

	return workflow.Apply(workflowActions)
}

// release tag
func tag(c *cli.Context) error {
	project := c.GlobalString("ci-project-path")
	sha := c.GlobalString("ci-commit-sha")
	client, err := clientFromContext(c)
	if err != nil {
		return errors.Wrap(err, "Unable to connect")
	}

	info, err := analyzeCommits(c)
	if err != nil {
		return err
	}
	if info.BumpLevel == semrel.NoBump {
		fmt.Printf("No changes found, since %s\n", info.CurrentVersion.String())
		return nil
	}

	releaseNote, err := render.ReleaseNote(info)
	if err != nil {
		return err
	}

	createTag := actions.NewCreateTag(
		client,
		project,
		actions.FuncOfString(sha),
		"v"+info.NextVersion.String(),
		releaseNote)

	return workflow.Apply([]workflow.Action{createTag})
}

func nextVersion(c *cli.Context) (*semrel.ReleaseData, error) {
    info, err := analyzeCommits(c)
	if err != nil {
		return errors.Wrap(err, "analyze commits")
	}
	if info.BumpLevel == semrel.NoBump {
	    if c.GlobalBool("major-version") {
	        info.NextVersion = info.CurrentVersion
	        info.NextVersion.Major++
	        info.NextVersion.Minor = 0
	        info.NextVersion.Patch = 0
	    }
		else if allowCurrent {
			info.NextVersion = info.CurrentVersion
		} else if bumpPatch {
			info.NextVersion = info.CurrentVersion
			info.NextVersion.Patch++
		} else {
			return errors.New("commit log didn't contain changes that would change the version")
		}
	}
	return info, err
}

// release next-version
// release test-git
func inspectAndPrint(c *cli.Context, version bool, releaseNote bool) error {
	bumpPatch := c.Bool("bump-patch")
	allowCurrent := c.Bool("allow-current")

	if bumpPatch && allowCurrent {
		return errors.New("bump-patch and allow-current are mutually exclusive")
	}

	info, err := nextVersion(c)
	if err != nil {
		return errors.Wrap(err, "analyze commits")
	}
	
	if version {
		fmt.Println(info.NextVersion.String())
	}
	if releaseNote {
		releaseNoteText, err := render.ReleaseNote(info)
		if err != nil {
			return err
		}
		fmt.Println(releaseNoteText)
	}
	return nil
}

// release changelog
func changelog(c *cli.Context) error {
	filename := c.String("f")

	if len(filename) == 0 {
		filename = "CHANGELOG.md"
	}

	info, err := analyzeCommits(c)
	if err != nil {
		return errors.Wrap(err, "analyze commits")
	}

	if info.BumpLevel == semrel.NoBump {
		return errors.New("no changes found in commit messages")
	}

	changelogEntry, err := render.ChangelogEntry(info)
	if err != nil {
		return err
	}

	if _, err := os.Stat(filename); os.IsNotExist(err) {
		// create a new changelog file
		fileparts := []string{
			"# CHANGELOG",
			"<!--- next entry here -->",
			changelogEntry,
		}
		data := strings.Join(fileparts, "\n\n")
		err = ioutil.WriteFile(filename, []byte(data), 0644)
		if err != nil {
			return errors.Wrap(err, "changelog, write file")
		}
		fmt.Printf("wrote %s\n", filename)
	} else {
		// insert new changelog entry to an existing file
		bytes, err := ioutil.ReadFile(filename)
		if err != nil {
			return errors.Wrap(err, "changelog, read file")
		}

		content := strings.Split(string(bytes), "\n\n<!--- next entry here -->\n\n")
		if len(content) != 2 {
			return errors.Errorf("Next entry -marker not found in %s", filename)
		}
		parts := []string{
			content[0],
			"<!--- next entry here -->",
			changelogEntry,
			content[1],
		}
		data := strings.Join(parts, "\n\n")

		err = ioutil.WriteFile(filename, []byte(data), 0644)
		if err != nil {
			return errors.Wrap(err, "changelog, write file")
		}
		fmt.Printf("updated %s\n", filename)
	}
	return nil
}

func main() {
	if len(version) == 0 {
		version = "DEV"
	}
	app := cli.NewApp()
	app.Usage = "semantic release tools for GitLab"
	app.Description = "A collection of utilities to help automate releases"
	app.Version = version
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "token, t",
			Usage:  "gitlab private `TOKEN`",
			EnvVar: "GITLAB_TOKEN,GL_TOKEN",
		},
		cli.StringFlag{
			Name:   "gl-api",
			Usage:  "gitlab api `URL`. (default is https://gitlab.com/api/v4/)",
			EnvVar: "GITLAB_URL,GL_URL",
		},
		cli.StringFlag{
			Name:   "ci-project-path",
			Usage:  "gitlab CI environment variable",
			Hidden: false,
			EnvVar: "CI_PROJECT_PATH",
		},
		cli.StringFlag{
			Name:   "ci-commit-sha",
			Usage:  "gitlab CI environment variable",
			Hidden: false,
			EnvVar: "CI_COMMIT_SHA",
		},
		cli.StringFlag{
			Name:   "ci-commit-tag",
			Usage:  "gitlab CI environment variable",
			Hidden: false,
			EnvVar: "CI_COMMIT_TAG",
		},
		cli.StringFlag{
			Name:   "ci-commit-ref-name",
			Usage:  "gitlab CI environment variable",
			Hidden: false,
			EnvVar: "CI_COMMIT_REF_NAME",
		},
		cli.BoolFlag{
			Name:   "skip-ssl-verify",
			Usage:  "don't verify CA certificate on gitlab api",
			Hidden: false,
		},
		cli.StringFlag{
			Name: "patch-commit-types",
			Usage: `comma separated list of commit message types that indicate patch bump
                               (default: "fix,refactor,perf,docs,style,test")`,
			Hidden: false,
			EnvVar: "SGS_PATCH_COMMIT_TYPES",
		},
		cli.StringFlag{
			Name: "minor-commit-types",
			Usage: `comma separated list of commit message types that indicate minor bump
                               (default: "feat")`,
			Hidden: false,
			EnvVar: "SGS_MINOR_COMMIT_TYPES",
		},
		cli.BoolFlag{
			Name:   "major-version",
			Usage:  "bump to next major version",
			Hidden: false,
			EnvVar: "CI_MAJOR_VERSION",
		},
	}
	app.Commands = []cli.Command{
		{
			Name:      "next-version",
			Usage:     "Print next version",
			UsageText: "release next-version [command options]",
			Description: `Analyze commits and print the next version. See 'release help tag'
   for more details on how the analysis works.

   The default is to fail, if no commits indicating a version bump are found.
   Use --bump-patch or --allow-current to alter default behaviour.

   --bump-patch and --allow-current are mutually exclusive.`,
			Action: func(c *cli.Context) error {
				return inspectAndPrint(c, true, false)
			},
			Flags: []cli.Flag{
				cli.BoolFlag{
					Name:  "bump-patch, p",
					Usage: "Bump patch number if no changes are found in log",
				},
				cli.BoolFlag{
					Name:  "allow-current, c",
					Usage: "Print current version number if no changes are found in log",
				},
			},
		},
		{
			Name:  "changelog",
			Usage: "Update changelog",
			Description: `Analyze commits and create or update changelog.
         
   HEAD's parents are traversed and changes are collected from commits
   that haven't been released yet.

   First encountered tag from each branch is compared and semantically
   latest is selected to be the base for calculating the next version.

   GitLab api and environment variables are not needed for this command.

   If commit messages indicating a version bump are not found, the command
   exits with non-zero value.`,
			Action: changelog,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "f",
					Usage: "Write changelog to `FILE` (default is CHANGELOG.md)",
				},
			},
		},
		{
			Name:  "tag",
			Usage: "Create tag and attach release note to HEAD",
			Description: `Analyze commits and create tag and release note.
         
   HEAD's parents are traversed and changes are collected from commits that
   haven't been released yet.

   First encountered tag from each branch is compared and semantically
   latest is selected to be the base for calculating the next version.

   Tag and release note are created using the collected information.

   If commit messages indicating a version bump are not found, the command
   exits with non-zero value.`,
			UsageText: "release tag",
			Action:    tag,
		},
		{
			Name:  "commit-and-tag",
			Usage: "Commit files and tag the new commit",
			Description: `Commit and push the listed files.
   If the files don't contain any changes, command fails.

   The commit message contains [skip ci], to prevent commit
   pipeline from running.

   Tag and release note are created for the new commit (check 'release help tag'
   for more details).

   [skip ci] also blocks tag pipe pipeline, so new pipeline is created to
   execute tag jobs.
     
   If commit messages indicating a version bump are not found, the command
   exits with non-zero value.`,
			UsageText: "release commit-and-tag [files to commit]",
			Action: func(c *cli.Context) error {
				return commitAndTagBase(c, false)
			},
		},
		{
			Name:  "tag-and-commit",
			Usage: "Tag HEAD and commit listed files",
			Description: `Attach tag and release note to HEAD (check
   'release help tag' for more details) and commit the listed files. 

   If the files don't contain any changes or commit messages indicating a
   version bump are not found, the command exits with non-zero value.

   The commit message contains [skip ci], to prevent commit pipeline from
   running.`,
			UsageText: "release tag-and-commit [files to commit]",
			Action: func(c *cli.Context) error {
				return commitAndTagBase(c, true)
			},
		},
		{
			Name:      "add-download",
			Usage:     "Add download to releasenote",
			UsageText: "release add-download [command options]",
			Description: `Upload file to project uploads and add link to release note.
            
   Requires CI_COMMIT_TAG environment variable or --ci-commit-tag flag.`,
			Action: addDownload,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "file, f",
					Usage: "`FILE` to add",
				},
				cli.StringFlag{
					Name:  "description, d",
					Usage: "file `DESCRIPTION`",
				},
			},
		},
		{
			Name:      "add-download-link",
			Usage:     "Add download link to release note",
			UsageText: "release add-download-link [command options]",
			Description: `Add download link to release note. Makes possible to host
   downloads anywhere.
     
   Requires CI_COMMIT_TAG environment variable or --ci-commit-tag flag.`,
			Action: addDownloadLink,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "name, n",
					Usage: "`TEXT` on the link",
				},
				cli.StringFlag{
					Name:  "url, u",
					Usage: "`URL` of the link",
				},
				cli.StringFlag{
					Name:  "description, d",
					Usage: "`DESCRIPTION` of the download",
				},
			},
		},
		{
			Name:  "test-git",
			Usage: "analyze git repository",
			Description: `Analyze unreleased commits and print release note.
         
   GitLab api and environment variables are not needed for this command`,
			UsageText: "release test-git",
			Action: func(c *cli.Context) error {
				return inspectAndPrint(c, false, true)
			},
		},
		{
			Name:      "test-api",
			Usage:     "test connection to GitLab api",
			UsageText: "release test-api",
			Action:    testAPI,
		},
		{
			Name:   "short-version",
			Usage:  "print just the version number",
			Action: shortVersion,
			Hidden: true,
		},
	}

	if cpuprof := os.Getenv("GO_SEMREL_GITLAB_CPUPROF"); cpuprof != "" {
		f, err := os.Create(cpuprof)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
