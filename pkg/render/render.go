package render

import (
	"bytes"
	"text/template"
	"time"
)

var (
	releaseNoteTmpl = `# {{ .NextVersion }}
{{ date }}{{ if .Changes.breaking }}

## Breaking changes{{ range .Changes.breaking }}

### {{ if ne "" .Scope }}**{{ .Scope }}:** {{ end}}{{ .Subject }} ({{ .Hash }})

{{ .BreakingMessage }}{{ end }}{{ end }}{{ if .Changes.feature }}

## Features
{{ range .Changes.feature }}
- {{ if ne "" .Scope }}**{{ .Scope }}:** {{ end}}{{ .Subject }} ({{ .Hash }}){{ end }}{{ end }}{{ if .Changes.fix }}

## Fixes
{{ range .Changes.fix }}
- {{ if ne "" .Scope }}**{{ .Scope }}:** {{ end}}{{ .Subject }} ({{ .Hash }}){{ end }}{{ end }}

<!--- downloads here -->`
	changelogTmpl = `## {{ .NextVersion }}
{{ date }}{{ if .Changes.breaking }}

### Breaking changes{{ range .Changes.breaking }}

#### {{ if ne "" .Scope }}**{{ .Scope }}:** {{ end}}{{ .Subject }} ({{ .Hash }})

{{ .BreakingMessage }}{{ end }}{{ end }}{{ if .Changes.feature }}

### Features
{{ range .Changes.feature }}
- {{ if ne "" .Scope }}**{{ .Scope }}:** {{ end}}{{ .Subject }} ({{ .Hash }}){{ end }}{{ end }}{{ if .Changes.fix }}

### Fixes
{{ range .Changes.fix }}
- {{ if ne "" .Scope }}**{{ .Scope }}:** {{ end}}{{ .Subject }} ({{ .Hash }}){{ end }}{{ end }}`
	funcs = template.FuncMap{"date": func() string { return time.Now().Format("2006-01-02") }}
)

func render(releaseInfo interface{}, tmpl string) (string, error) {
	t := template.Must(template.New("release").Funcs(funcs).Parse(tmpl))
	buf := bytes.Buffer{}
	err := t.Execute(&buf, releaseInfo)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}

// ReleaseNote takes release info object returned by semrel.Release and returns markdown for the release note
func ReleaseNote(releaseInfo interface{}) (string, error) {
	return render(releaseInfo, releaseNoteTmpl)
}

// ChangelogEntry takes release info object returned by semrel.Release and returns markdown for the changelog entry
func ChangelogEntry(releaseInfo interface{}) (string, error) {
	return render(releaseInfo, changelogTmpl)
}
