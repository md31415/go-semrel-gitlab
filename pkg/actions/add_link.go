package actions

import (
	"fmt"
	"strings"

	"github.com/pkg/errors"
	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/juhani/go-semrel-gitlab/pkg/gitlabutil"
	"gitlab.com/juhani/go-semrel-gitlab/pkg/workflow"
)

// AddLinkParams ..
type AddLinkParams struct {
	Client          *gitlab.Client
	Project         string
	LinkDescription string
	// MDLinkFunc returns markdown link of form '[link text](https://foo.bar....)'
	MDLinkFunc func() string
	TagFunc    func() *gitlab.Tag
}

// AddLink action
type AddLink struct {
	*AddLinkParams
	origRelNote string
}

// Do implements Action for AddLink
func (action *AddLink) Do() *workflow.ActionError {
	if len(action.origRelNote) > 0 {
		return nil
	}
	markdownLink := action.MDLinkFunc()
	tag := action.TagFunc()

	newRelNote := addDownloadLinkToDescription(tag.Release.Description, markdownLink, action.LinkDescription)

	_, resp, err := gitlabutil.UpdateTagDescription(action.Client, action.Project, tag.Name, newRelNote)
	if err != nil {
		retry := false
		if resp.StatusCode == 502 {
			retry = true
		}
		return workflow.NewActionError(errors.Wrap(err, "update description"), retry)
	}
	action.origRelNote = tag.Release.Description
	return nil
}

// Undo implements Action for AddLink
func (action *AddLink) Undo() error {
	if len(action.origRelNote) == 0 {
		return nil
	}
	tag := action.TagFunc()

	_, _, err := gitlabutil.UpdateTagDescription(action.Client, action.Project, tag.Name, action.origRelNote)
	if err != nil {
		fmt.Printf(`
MANUAL ACTION REQUIRED!!
Restoring the description for tag %s failed.
The text of the original description is:
-----
%s
-----\n`, tag.Name, action.origRelNote)
		return errors.Wrap(err, "update description")
	}
	action.origRelNote = ""
	return nil
}

func addDownloadLinkToDescription(origReleaseText string, markdownLink string, description string) string {
	parts := []string{}
	marker := "\n<!--- download here -->"

	if strings.Index(origReleaseText, "<!--- downloads here -->") > -1 {
		parts = append(parts, "### Downloads", "")
		marker = "<!--- downloads here -->"
	} else if strings.Index(origReleaseText, "<!--- download here -->") == -1 {
		// marker was not found, add it to the end of release note
		origReleaseText += marker
	}

	parts = append(parts, fmt.Sprintf("- **%s:** %s", markdownLink, description), "", "<!--- download here -->")
	return strings.Replace(origReleaseText, marker, strings.Join(parts, "\n"), 1)
}

// NewAddLink ..
func NewAddLink(params *AddLinkParams) *AddLink {
	return &AddLink{
		AddLinkParams: params,
	}
}
