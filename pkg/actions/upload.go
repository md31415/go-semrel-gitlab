package actions

import (
	"fmt"

	"github.com/pkg/errors"
	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/juhani/go-semrel-gitlab/pkg/workflow"
)

// Upload ..
type Upload struct {
	client      *gitlab.Client
	project     string
	file        string
	projectFile *gitlab.ProjectFile
}

// Do implements Action for Upload
func (action *Upload) Do() *workflow.ActionError {
	if action.projectFile != nil {
		return nil
	}
	projectFile, resp, err := action.client.Projects.UploadFile(action.project, action.file)
	if err != nil {
		retry := false
		if resp.StatusCode == 502 {
			retry = true
		}
		return workflow.NewActionError(errors.Wrap(err, "upload file"), retry)
	}
	action.projectFile = projectFile
	return nil
}

// Undo implements Action for Upload
func (action *Upload) Undo() error {
	if action.projectFile == nil {
		return nil
	}
	fmt.Printf("File upload cannot be rolled back: %s.\n", action.file)
	return errors.Errorf("file upload cannot be rolled back: %s", action.file)
}

// MDLinkFunc returns an accessor function to project file markdown link
func (action *Upload) MDLinkFunc() func() string {
	return func() string {
		if action.projectFile == nil {
			return ""
		}
		return action.projectFile.Markdown
	}
}

// NewUpload ..
func NewUpload(client *gitlab.Client, project string, file string) *Upload {
	return &Upload{
		client:  client,
		project: project,
		file:    file,
	}
}
